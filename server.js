const core = require('./dist/core');
const express = require('./config/express');
const logger = require('./packages/utilities/logger');
const swagger = require('./config/swagger');

const MODULENAME = 'SERVER';

const server = express();
swagger(server);

core.init(server)
    .then(_ => {
        server.listen(3000, err => {
            err
            ? logger.error(MODULENAME, err)
            : logger.info(MODULENAME, `Server running on Port ${3000}`);
        });
    })
    .catch(err => logger.error(MODULENAME, err))
