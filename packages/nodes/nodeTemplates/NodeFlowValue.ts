'use strict';

import { NodeBase } from '../nodeBase';
import Flow from '../../flow/lib/Flow';
import { InputPort, OutputPort } from '../ports';
import { PortValueType, portTypeFactory } from '../ports/PortTypes';
import logger from '../../utilities/logger';

const MODULENAME = 'NodeFlowValue';

export default class NodeFlowValue extends NodeBase {
  constructor(parentFlow: Flow, nodeId: string) {
    super(parentFlow, nodeId);

    this.addPort(new OutputPort(this, portTypeFactory.createTriggerPortType(), 'trigger', 0));

    this.addPort(new OutputPort(
      this,
      portTypeFactory.createNumberPortType(),
      'latestMeasurement',
      () => this.getLatestMeasurement())
    );

    if (parentFlow) parentFlow.registerOnValueUpdateCB(this.triggerNext.bind(this));
  }

  getTriggerOutputPort(): OutputPort {
    return this.getOutputPorts()
      .find(outputPort => outputPort.getPortType().type === PortValueType.trigger);
  }

  triggerNext() {
    const triggerOutputPort = this.getTriggerOutputPort();
    if (triggerOutputPort) {
      const connectedPort = triggerOutputPort.getConnectedPort() as InputPort;
      if (!connectedPort) return logger.warning(MODULENAME, 'Trigger port not connected - canceling...');
      connectedPort.parentNode.start();
    }
  }

  getLatestMeasurement() {
    return this.parentFlow.getCurrentValue();
  }
}
