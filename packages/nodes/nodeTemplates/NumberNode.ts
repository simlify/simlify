'use strict';

import { NodeDataBase } from '../nodeBase';
import { OutputPort } from '../ports';
import { portTypeFactory } from '../ports/portTypes';
import Flow from '../../flow/lib/Flow';

export default class NumberNode extends NodeDataBase {
  value: number;

  constructor(parentFlow: Flow, nodeId: string) {
    super(parentFlow, nodeId);
    this.value = 1000;
    super.addPort(new OutputPort(
        this,
        portTypeFactory.createNumberPortType(),
        'number',
        () => this.value
    ));
  }
}
