import React from 'react';

import './Header.scss';

const Header = () => {
  return (
    <div className="header">
      <div>Logo</div>
      <div>Menu</div>
    </div>
  );
}

export default Header;
