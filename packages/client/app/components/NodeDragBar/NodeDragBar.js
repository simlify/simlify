import React, { useState, useEffect } from 'react';
import Paper from '../Paper';
import { withStyles } from '@material-ui/core/styles';

const styles = {
};

const DragBar = (props) => {
  return (
    <div
      draggable={true}
      onDragStart={event => {
        console.log(event);
        event.dataTransfer.setData('diagram-node', JSON.stringify(props.nodeData));
      }}
      style={{border: 'solid 1px gray', padding: '5px', 'backgroundColor': '#2e436b', 'color': 'white'}}
    >
      {props.children}
    </div>
  );
}

const NodeDragBar = (props) => {
  const { availableNodes } = props;
  const dragBar = Object.entries(availableNodes).map(([nodeName, nodeData]) => <DragBar nodeData={nodeData}>{nodeName}</DragBar>);
  return (
    <Paper>
      { dragBar }
    </Paper>
  );
};

export default withStyles(styles)(NodeDragBar);
