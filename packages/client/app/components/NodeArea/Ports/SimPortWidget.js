import * as React from 'react';
import { PortWidget } from '@projectstorm/react-diagrams-core';
import PortTextField from '../../PortTextField';

import './SimPortStyle.scss';

export class SimPortLabel extends React.Component {
	render() {
		const { port, engine } = this.props;
		const isConnected = !!Object.keys(port.links).length;

        const { color: portBackgroundColor, isEditable, type } = port.portType;

		const portElem = (
			<PortWidget engine={engine} port={port}>
				<div className="simPortLabel__port" style={{ backgroundColor: portBackgroundColor }} />
			</PortWidget>
		);
        const labelElem = <div className="simPortLabel__label">
            { 
                isEditable &&
                port.direction === 'in' &&
                <PortTextField disabled={isConnected} defaultValue={port.portType.value} onChange={(value) => port.portType.value = value}/>
            }
            { !this.props.disableLabel && port.options.label }
        </div>;

		return (
			<div className="simPortLabel">
				{port.direction === 'in' ? portElem : labelElem}
				{port.direction === 'in' ? labelElem : portElem}
			</div>
		);
	}
}