import * as React from 'react';
import { SimPortLabel } from '../Ports/SimPortWidget';

import './SimulationNodeStyle.scss';

export class SimulationNodeWidget extends React.Component {
	render() {
		const { node } = this.props;
		const ports = node.getPorts();

		const inputPorts = [];
		const outputPorts = [];
		let triggerInputPort = null;
		let triggerOutputPort = null;

		Object.values(ports).forEach((port) => {
			port.setMaximumLinks(1);
			if (port.direction === 'in') {
				if(port.portType.type === 'trigger') triggerInputPort = port;
				else inputPorts.push(port);
			} 
			else {
				if(port.portType.type === 'trigger') triggerOutputPort = port;
				else outputPorts.push(port);
			}
		})

		return (
			<div className={`simulationNode ${node.isSelected() ? 'simulationNode--selected' : ''}`}>
				<div className="simulationNode__header">
					<div className="simulationNode__titleNode">
						{
							triggerInputPort
							? <SimPortLabel disableLabel engine={this.props.engine} port={triggerInputPort} />
							: <div />
						}
						<div className="simulationNode__titleNode__title"> {node.options.name} </div>
						{
							triggerOutputPort
							? <SimPortLabel disableLabel engine={this.props.engine} port={triggerOutputPort} />
							: <div />
						}
					</div>
				</div>
				<div className="simulationNode__body">
					<div className="simulationNode__ports simulationNode__inputPorts">
					{
						inputPorts.map((inputPort) => (
							<SimPortLabel engine={this.props.engine} port={inputPort} />
						))
					}
					</div>
					<div className="simulationNode__ports simulationNode__outputPorts">
					{
						outputPorts.map((outputPort) => (
							<SimPortLabel engine={this.props.engine} port={outputPort} />
						))
					}
					</div>
				</div>
			</div>
		);
	}
}